package ovrbach.com.zadaniegl;
//:*

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLHelper extends SQLiteOpenHelper {

public static final String TABLE_ARTICLE ="article";
    public static final String Article_id="id";
    public static final String Article_headline="headline";
    public static final String Article_body="body";
    public static final String Article_leaderImage="leaderImage";
    public static final String Article_leaderPictureCredit="leaderPictureCredit";
    public static final String Article_linkOneUrl="linkOneUrl";
    public static final String Article_linkOneTitle="linkOneTitle";
    public static final String Article_shareLink="shareLink";
    public static final String Article_chart="chart";
    public static final String Article_nid="nid";
    public static final String Article_nHash="nHash";
    public static final String Article_owner="owner";
    public static String Article_type="type";

    public  static final String CREATE_TABLE_ARTICLE = "create table "+ TABLE_ARTICLE +" ("
            +Article_id+" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+Article_type +" TEXT, "+Article_headline+
            " TEXT, " + Article_body +" TEXT, "+Article_leaderImage+" BLOB, "+Article_leaderPictureCredit+" TEXT, "+
             Article_linkOneUrl+" TEXT, "+Article_linkOneTitle+" TEXT, "+Article_shareLink
            +" TEXT, "+ Article_chart +" INTEGER, "+ Article_nid + " INTEGER, "+Article_nHash+" TEXT, "+Article_owner
            +" TEXT, UNIQUE("+Article_nid+"));";

    //foreign key pouzit?
    //alebo tam smarit rovno celu bundle
    public static final String [] ARTICLE_COLUMNS={
            Article_id,
            Article_type,
            Article_headline,
            Article_body,
            Article_leaderImage,
            Article_leaderPictureCredit,
            Article_linkOneUrl,
            Article_linkOneTitle,
            Article_shareLink,
            Article_chart,
            Article_nid,
            Article_nHash,
            Article_owner
    };


public static final String TABLE_ISSUE ="issue";
    public static final String Issue_id="id";
    public static final String Issue_type="type";
    public static final String Issue_region="region";
    public static final String Issue_issueDate="issueDate";
    public static final String Issue_nid="nid";
    public static final String Issue_updated="updated";
    public static final String Issue_headlineSum="headlineSum";
    public static final String Issue_zinger="zinger";
    public static final String Issue_zingerAuthor="zingerAuthor";
    public static final String Issue_dataTimeStamp="dataTimeStamp";
    public static final String Issue_owner="owner";
    public static final String Issue_marketDataType="marketDataType";
    public static final String Issue_footerNote="footerNote";
    public static final String Issue_articles="articles";


    public  static final String CREATE_TABLE_ISSUE = "create table "+ TABLE_ISSUE +" ("
            +Issue_id+" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+Issue_type+
            " TEXT, " + Issue_region +" TEXT, "+Issue_issueDate+" DATETIME, "+Issue_nid+" INTEGER, "+
            Issue_updated +" DATETIME, "+ Issue_headlineSum+" TEXT, "+Issue_zinger+" TEXT, "+Issue_zingerAuthor
            +" TEXT, "+ Issue_dataTimeStamp +" INTEGER, "+ Issue_owner + " TEXT, "+Issue_marketDataType+" TEXT, "
            +Issue_footerNote +" TEXT, "+Issue_articles +" TEXT, UNIQUE("+Issue_nid+"));";


    public static final String [] ISSUE_COLUMNS= {
            Issue_id,
            Issue_type,
            Issue_region,
            Issue_issueDate,
            Issue_nid,
            Issue_updated,
            Issue_headlineSum,
            Issue_zinger,
            Issue_zingerAuthor,
            Issue_dataTimeStamp,
            Issue_owner,
            Issue_marketDataType,
            Issue_footerNote,
            Issue_articles
    };




    public SQLHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public SQLHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    public SQLHelper(Context context) {
        super(context, TABLE_ARTICLE, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE_ARTICLE);
        db.execSQL(CREATE_TABLE_ISSUE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTICLE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ISSUE);
        onCreate(db);
    }


}
