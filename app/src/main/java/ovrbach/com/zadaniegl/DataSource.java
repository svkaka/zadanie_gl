package ovrbach.com.zadaniegl;
//:*

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static ovrbach.com.zadaniegl.SQLHelper.Article_body;
import static ovrbach.com.zadaniegl.SQLHelper.Article_headline;

import static ovrbach.com.zadaniegl.SQLHelper.Article_leaderImage;
import static ovrbach.com.zadaniegl.SQLHelper.Article_leaderPictureCredit;
import static ovrbach.com.zadaniegl.SQLHelper.Article_linkOneTitle;
import static ovrbach.com.zadaniegl.SQLHelper.Article_linkOneUrl;
import static ovrbach.com.zadaniegl.SQLHelper.Article_nHash;
import static ovrbach.com.zadaniegl.SQLHelper.Article_nid;
import static ovrbach.com.zadaniegl.SQLHelper.Article_owner;
import static ovrbach.com.zadaniegl.SQLHelper.Article_shareLink;
import static ovrbach.com.zadaniegl.SQLHelper.Article_type;
import static ovrbach.com.zadaniegl.SQLHelper.Issue_articles;
import static ovrbach.com.zadaniegl.SQLHelper.Issue_dataTimeStamp;
import static ovrbach.com.zadaniegl.SQLHelper.Issue_footerNote;
import static ovrbach.com.zadaniegl.SQLHelper.Issue_headlineSum;
import static ovrbach.com.zadaniegl.SQLHelper.Issue_issueDate;
import static ovrbach.com.zadaniegl.SQLHelper.Issue_marketDataType;
import static ovrbach.com.zadaniegl.SQLHelper.Issue_nid;
import static ovrbach.com.zadaniegl.SQLHelper.Issue_owner;
import static ovrbach.com.zadaniegl.SQLHelper.Issue_region;
import static ovrbach.com.zadaniegl.SQLHelper.Issue_updated;
import static ovrbach.com.zadaniegl.SQLHelper.Issue_zinger;
import static ovrbach.com.zadaniegl.SQLHelper.Issue_zingerAuthor;

public class DataSource {
    private SQLiteDatabase database;
    private SQLHelper dbHelper;


    public DataSource(Context context) {
        dbHelper = new SQLHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }


    public Article getArticlesById(Long id) {
        Cursor cursor = database.query(SQLHelper.TABLE_ARTICLE,
                SQLHelper.ARTICLE_COLUMNS, Article_nid + " = " + id, null, null, null, null);
            cursor.moveToFirst();
            Article article = cursorToArticle(cursor);
            cursor.close();
            return article;
}


    public Issue getIssueById(Long id) {
        Cursor cursor = database.query(SQLHelper.TABLE_ISSUE,
                SQLHelper.ISSUE_COLUMNS, Issue_nid + " = " + id, null, null, null, null);

            cursor.moveToFirst();
            Issue issue = cursorToIssue(cursor);
            cursor.close();
            return issue;
        }


    public ArrayList<Article> getAllArticles() {
        ArrayList<Article> articleList = new ArrayList<>();
        Cursor cursor = database.query(SQLHelper.TABLE_ARTICLE,
                SQLHelper.ARTICLE_COLUMNS, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            articleList.add(cursorToArticle(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return articleList;
    }

    public ArrayList<Issue> getAllIssues() {
        ArrayList<Issue> issues = new ArrayList<>();
        Cursor cursor = database.query(SQLHelper.TABLE_ISSUE,
                SQLHelper.ISSUE_COLUMNS, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            issues.add(cursorToIssue(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return issues;
    }


    private void insertArticleIntoDBS(ContentValues values) {
     //   database.insert(SQLHelper.TABLE_ARTICLE, null, values);
        database.insertWithOnConflict(SQLHelper.TABLE_ARTICLE, null, values,SQLiteDatabase.CONFLICT_IGNORE);
    }

    private void insertIssueIntoDBS(ContentValues values) {
        database.insertWithOnConflict(SQLHelper.TABLE_ISSUE, null, values,SQLiteDatabase.CONFLICT_IGNORE);
    }


    public void deleteArticle(Article article) {
        database.delete(SQLHelper.TABLE_ARTICLE, Article_nid
                + " = " + article.getNid(), null);
    }

    public void deleteArticleById(Long id) {
        database.delete(SQLHelper.TABLE_ARTICLE, Article_nid
                + " = " + id, null);
    }

    public void deleteIssue(Issue issue) {
        for (Long id : issue.getArticlesIds()) {
            deleteArticleById(id);
        }
        database.delete(SQLHelper.TABLE_ISSUE, Issue_nid
                + " = " + issue.getNid(), null);

    }


    public void updateArticle(ContentValues values, String filter) {
        database.update(SQLHelper.TABLE_ARTICLE, values, filter, null);
    }

    public void updateIssue(ContentValues values, String filter) {
        database.update(SQLHelper.TABLE_ISSUE, values, filter, null);
    }

    //todo open by hash


    private Article cursorToArticle(Cursor cursor) {
        Article article = new Article();

        if (cursor.getCount() > 0) {
            article.setDbsId(cursor.getInt(0));
            article.setType(cursor.getString(1));
            article.setHeadline(cursor.getString(2));
            article.setBody(cursor.getString(3));
            article.setLeaderImage(cursor.getBlob(4));
            article.setLeaderPictureCredit(cursor.getString(5));
            article.setLinkOneUrl(String.valueOf(cursor.getInt(6)));
            article.setLinkOneTitle(cursor.getString(7));
            article.setShareLink(cursor.getString(8));
            article.setChart(cursor.getInt(9));
            article.setNid(cursor.getLong(10));
            article.setNhash(cursor.getString(11));
            article.setOwner(cursor.getString(12));
        }
        return article;
    }

    private Issue cursorToIssue(Cursor cursor) {
        Issue issue = new Issue();
        SimpleDateFormat i_date = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat u_date = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        if (cursor.getCount() > 0) {
            issue.setDbsId(cursor.getInt(0));
            issue.setType(cursor.getString(1));
            issue.setRegion(cursor.getString(2));

            try {
                issue.setIssueDate(i_date.parse(cursor.getString(3)));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            issue.setNid(cursor.getLong(4));

            try {
                issue.setUpdated(u_date.parse(cursor.getString(5)));
            } catch (NullPointerException | ParseException e) {
                //  e.printStackTrace();
            }

            issue.setHeadlineSum(cursor.getString(6));
            issue.setZinger(cursor.getString(7));
            issue.setZingerAuthor(cursor.getString(8));
            issue.setDataTimeStamp(cursor.getLong(9));
            issue.setOwner(cursor.getString(10));
            issue.setMarketDataType(cursor.getString(11));
            issue.setFooterNote(cursor.getString(12));

            try {
                JSONObject json = new JSONObject(cursor.getString(13));
                JSONArray jArray = json.optJSONArray("aid");
                for (int i = 0; i < jArray.length(); i++) {
                    issue.getArticles().add(getArticlesById(jArray.optLong(i)));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return issue;
    }


    void insertArticle(Article article) {
        //if (getArticlesById(article.getNid()) == null) {
//todo nenastavovat zbytocne ked to nevlozim
            ContentValues values = new ContentValues();
            values.put(Article_type, article.getType());
            values.put(Article_headline, article.getHeadline());
            values.put(Article_leaderImage, article.getLeaderImage());
            values.put(Article_body, article.getBody());
            values.put(Article_leaderPictureCredit, article.getLeaderPictureCredit());
            values.put(Article_linkOneUrl, article.getLinkOneUrl());
            values.put(Article_linkOneTitle, article.getLinkOneTitle());
            values.put(Article_shareLink, article.getShareLink());
            values.put(Article_nid, article.getNid());
            values.put(Article_nHash, article.getNhash());
            values.put(Article_owner, article.getOwner());

            insertArticleIntoDBS(values);

    }



    void insertIssue(Issue issue) {
//if (getIssueById(issue.getNid())!=null) return;
            ContentValues values = new ContentValues();
            SimpleDateFormat i_date = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat u_date = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            values.put(Issue_region, issue.getRegion());
            values.put(Issue_issueDate, String.valueOf(i_date.format(issue.getIssueDate())));
            try {
                values.put(Issue_updated, String.valueOf(u_date.format(issue.getUpdated())));
            } catch (NullPointerException ignored) {

            }
            values.put(Issue_nid, issue.getNid());
            values.put(Issue_headlineSum, issue.getHeadlineSum());
            values.put(Issue_zinger, issue.getZinger());
            values.put(Issue_zingerAuthor, issue.getZingerAuthor());
            values.put(Issue_dataTimeStamp, issue.getDataTimeStamp());
            values.put(Issue_owner, issue.getOwner());
            values.put(Issue_marketDataType, issue.getMarketDataType());
            values.put(Issue_footerNote, issue.getFooterNote());

            try {
                JSONObject json = new JSONObject();
                json.put("aid", new JSONArray(issue.getArticlesIds()));
                String arrayList = json.toString();
                values.put(Issue_articles, arrayList);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            insertIssueIntoDBS(values);

    }

}
