package ovrbach.com.zadaniegl;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private FirebaseAnalytics mFirebaseAnalytics;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private static DataSource dataSource;

    private ArrayList<Issue> issues = new ArrayList<>();
    private ImageView mParalaxImg;
    private TabLayout tabLayout;

    private TextView mhead, mbody;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "sth");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "name");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        dataSource = new DataSource(this);
        try {
            dataSource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        LoadData ld = new LoadData();
        ld.execute();

    }



    /*@Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event) {
        System.out.println("UAZSDGAUSZDGA");
    }*/

    private void changeTab(int currentPage) {
        mViewPager.setCurrentItem(currentPage);

        EventBus.getDefault().post(new MessageEvent(issues));
        Issue i = issues.get(currentPage);
        Article a = i.getArticles().get(0);
        Glide.with(this)
                .load(a.getLeaderImage())
                .animate(R.anim.slide_up)
                //.asBitmap()
                .into(mParalaxImg);

        mhead.setText(a.getHeadline());
        mbody.setText(Html.fromHtml(a.getBody()));
    }


    private void loadFromDBS() {
        System.out.println("LOADING FROM DBS");
        issues = dataSource.getAllIssues();
//        mSectionsPagerAdapter.notifyDataSetChanged();
    }


    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private int num;

        public PlaceholderFragment() {
            if (!eventBus.isRegistered(this)) {
                eventBus.register(this);
            }
            eventBus.post(new MessageAmReady());
        }

        RecyclerView recyclerView;

        EventBus eventBus = EventBus.getDefault();

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {

            PlaceholderFragment fragment = new PlaceholderFragment();

            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);

            return fragment;
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.main_pager_fragment, container, false);
            num = getArguments().getInt(ARG_SECTION_NUMBER);

            recyclerView = (RecyclerView) rootView.findViewById(R.id.rv);
            LinearLayoutManager lm = new LinearLayoutManager(this.getContext());
            recyclerView.setLayoutManager(lm);

            return rootView;
        }

        //todo nebudit vsetkych uzbyocne
        @Subscribe(threadMode = ThreadMode.MAIN)
        public void onEvent(MessageEvent event) {
            if (recyclerView != null) {
                ArticlesAdapter adpater = new ArticlesAdapter(event.issues.get(num).getArticles());
                recyclerView.setAdapter(adpater);
            }
        }

    }
//toto oninstane

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return issues.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Issue i = issues.get(position);
            //SimpleDateFormat i_date = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat p_date = new SimpleDateFormat("EEE.\nMMMM dd");
            return p_date.format(i.getIssueDate());
        }
    }


    private static class MessageEvent {
        private ArrayList<Issue> issues;

        MessageEvent(ArrayList<Issue> issues) {
            this.issues = issues;
        }
    }

    private static class MessageAmReady {
        MessageAmReady() {
        }
    }

    private void loadFiles() {
        String fnames[] = new String[0];
        try {
            fnames = getAssets().list("");
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (String fname : fnames) {
            try {
                processGsonFromFile(fname);
            } catch (IOException e) {
                // e.printStackTrace();
            }
        }
    }

    void processGsonFromFile(String f) throws IOException {
        BufferedReader reader =new BufferedReader(new InputStreamReader(getAssets().open(f)));
        Gson gson = new Gson();

        JsonParser parser = new JsonParser();
        JsonArray array = parser.parse(reader).getAsJsonArray();
        Issue issue = null;

        for (int i = 0; i < array.size(); i++) {
            if (i == 0) {
                issue = gson.fromJson(array.get(i), Issue.class);
            } else {
                try {
                    Article article = GsonHelper.customGson.fromJson(array.get(i), Article.class);

                    if (article.getType().equals("article")) {
                        issue.getArticlesIds().add(article.getNid());
                        issue.getArticles().add(article);

                        dataSource.insertArticle(article);
                    } else {
                        ;;;
                    }

                    //todo spracovat aj ostatne typy
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

        }
        dataSource.insertIssue(issue);
        issues.add(issue);
        // mSectionsPagerAdapter.notifyDataSetChanged();
    }

    //fakt narychlo
    private class LoadData extends AsyncTask<Void, Void, Void> {

        private ProgressDialog progDailog;

        private LoadData() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(MainActivity.this);
            progDailog.setMessage("Loading...");
            progDailog.setIndeterminate(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(true);
            progDailog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            loadFiles();
            loadFromDBS();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

            mViewPager = (ViewPager) findViewById(R.id.container);
            mViewPager.setPageTransformer(true, new DepthPageTransformer());
            mViewPager.setAdapter(mSectionsPagerAdapter);
            tabLayout = (TabLayout) findViewById(R.id.tabs);

            mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    changeTab(position);
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
            tabLayout.setupWithViewPager(mViewPager);
            mhead = (TextView) findViewById(R.id.main_headline);
            mbody = (TextView) findViewById(R.id.main_body);
            mParalaxImg = (ImageView) findViewById(R.id.parallax_image);
/*


            loadFiles();
//todo tie co uz su netahat
            loadFromDBS();
*/

            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    changeTab(tab.getPosition());
                    //mViewPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                    //  mViewPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                    //  mViewPager.setCurrentItem(tab.getPosition());
                }
            });
            changeTab(tabLayout.getSelectedTabPosition());
            //EventBus.getDefault().postSticky(new MessageEvent(issues));
            //post(new MessageEvent(issues));
            progDailog.dismiss();
        }
    }


}


