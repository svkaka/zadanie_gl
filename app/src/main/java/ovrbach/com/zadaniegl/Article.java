package ovrbach.com.zadaniegl;
//:*

public class Article {
    private String headline;
    private String body;
    private byte[] leaderImage;
    private String leaderPictureCredit;
    private String linkOneUrl;
    private String linkOneTitle;
    private String shareLink;
    private int chart;
    private Long nid;
    private String nhash;
    private String owner;
    private String type;
private long dbsId;


    public String getNhash() {
        return nhash;
    }

    public void setNhash(String nhash) {
        this.nhash = nhash;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public byte[] getLeaderImage() {
        return leaderImage;
    }

    public void setLeaderImage(byte[] leaderImage) {
        this.leaderImage = leaderImage;
    }

    public String getLeaderPictureCredit() {
        return leaderPictureCredit;
    }

    public void setLeaderPictureCredit(String leaderPictureCredit) {
        this.leaderPictureCredit = leaderPictureCredit;
    }

    public String getLinkOneUrl() {
        return linkOneUrl;
    }

    public void setLinkOneUrl(String linkOneUrl) {
        this.linkOneUrl = linkOneUrl;
    }

    public String getLinkOneTitle() {
        return linkOneTitle;
    }

    public void setLinkOneTitle(String linkOneTitle) {
        this.linkOneTitle = linkOneTitle;
    }

    public String getShareLink() {
        return shareLink;
    }

    public void setShareLink(String shareLink) {
        this.shareLink = shareLink;
    }

    public int getChart() {
        return chart;
    }

    public void setChart(int chart) {
        this.chart = chart;
    }

    public Long getNid() {
        return nid;
    }

    public void setNid(Long nid) {
        this.nid = nid;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public long getDbsId() {
        return dbsId;
    }

    public void setDbsId(long dbsId) {
        this.dbsId = dbsId;
    }

    @Override
    public String toString() {
        return "Article{" +
                "headline='" + headline + '\'' +
                ", body='" + body + '\'' +
                ", leaderPictureCredit='" + leaderPictureCredit + '\'' +
                ", linkOneUrl='" + linkOneUrl + '\'' +
                ", linkOneTitle='" + linkOneTitle + '\'' +
                ", shareLink='" + shareLink + '\'' +
                ", chart=" + chart +
                ", nid=" + nid +
                ", nhash='" + nhash + '\'' +
                ", owner='" + owner + '\'' +
                ", type='" + type + '\'' +
                ", dbsId=" + dbsId +
                '}';
    }
}
