package ovrbach.com.zadaniegl;
//:*

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ViewHolder> {

    private ArrayList<Article> data=new ArrayList<>();

public ArticlesAdapter(ArrayList<Article> data) {
        this.data = data;
        }

@Override
public ArticlesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_item_small, parent, false);
    return new ViewHolder(v);
        }

@Override
public void onBindViewHolder(final ViewHolder holder, final int position) {
    final Article article=data.get(position);

//   byte[] imageByteArray = Base64.decode(article.getLeaderImage(), Base64.DEFAULT);
    Glide.with(holder.itemView.getContext())
            .load(article.getLeaderImage())
            .asBitmap()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(holder.img);

  // holder.strip.setBackground(R.color.colorPrimary);
    holder.title.setText(article.getHeadline());
    try {
        holder.zing.setText(Html.fromHtml(article.getBody()));
    }catch (NullPointerException e){
        holder.zing.setText(article.getBody());
    }

  //  holder.zing.setText("ASIDJAISDJASID");
   holder.cat.setText(article.getHeadline());

    holder.itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(holder.itemView.getContext(), DeatailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putLong("NIB", article.getNid());
            intent.putExtras(bundle);
           holder.itemView.getContext().startActivity(intent);
        }
    });

}


@Override
public int getItemCount() {
//    System.out.println(data.size()+"DATATA SIZE");
        return data.size();
        }

public static class ViewHolder extends RecyclerView.ViewHolder {
   // SummaryItemBinding binding;
   // private final Context context;

    private View strip;
    private ImageView img;
    private TextView title,cat,zing;

    ViewHolder(View itemView) {
        super(itemView);
        strip=itemView.findViewById(R.id.item_main_small_strip);
        img= (ImageView) itemView.findViewById(R.id.item_main_small_img);
        title= (TextView) itemView.findViewById(R.id.item_main_small_title);
        cat= (TextView) itemView.findViewById(R.id.item_main_small_cat);
        zing= (TextView) itemView.findViewById(R.id.item_main_small_short);


    }


}
}

    

