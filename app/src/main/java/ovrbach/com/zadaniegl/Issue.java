package ovrbach.com.zadaniegl;
//:*

import java.util.ArrayList;
import java.util.Date;

public class Issue {
    private String type;
    private String region;
    private Date issueDate;
    private long nid;
    private Date updated;

    private String headlineSummary;
    private String zinger;
    private String zingerAuthor;

    private long dataTimestamp;
    private String owner;

    private String marketDataType;
    private String footerNote;

    private ArrayList<Article> articles=new ArrayList<>();
    private ArrayList<Long> articlesIds=new ArrayList<>();

    private long dbsId;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public long getNid() {
        return nid;
    }

    public void setNid(long nid) {
        this.nid = nid;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getHeadlineSum() {
        return headlineSummary;
    }

    public void setHeadlineSum(String headlineSum) {
        this.headlineSummary = headlineSum;
    }

    public String getZinger() {
        return zinger;
    }

    public void setZinger(String zinger) {
        this.zinger = zinger;
    }

    public String getZingerAuthor() {
        return zingerAuthor;
    }

    public void setZingerAuthor(String zingerAuthor) {
        this.zingerAuthor = zingerAuthor;
    }

    public long getDataTimeStamp() {
        return dataTimestamp;
    }

    public void setDataTimeStamp(long dataTimeStamp) {
        this.dataTimestamp = dataTimeStamp;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getMarketDataType() {
        return marketDataType;
    }

    public void setMarketDataType(String marketDataType) {
        this.marketDataType = marketDataType;
    }

    public String getFooterNote() {
        return footerNote;
    }

    public void setFooterNote(String footerNote) {
        this.footerNote = footerNote;
    }

    public ArrayList<Article> getArticles() {
        return articles;
    }

    public void setArticles(ArrayList<Article> articles) {
        this.articles = articles;
    }

    public long getDbsId() {
        return dbsId;
    }

    public void setDbsId(long dbsId) {
        this.dbsId = dbsId;
    }

    public ArrayList<Long> getArticlesIds() {
        return articlesIds;
    }

    public void setArticlesIds(ArrayList<Long> articlesIds) {
        this.articlesIds = articlesIds;
    }

    @Override
    public String toString() {
        return "Issue{" +
                "type='" + type + '\'' +
                ", region='" + region + '\'' +
                ", issueDate=" + issueDate +
                ", nid=" + nid +
                ", updated=" + updated +
                ", headlineSummary='" + headlineSummary + '\'' +
                ", zinger='" + zinger + '\'' +
                ", zingerAuthor='" + zingerAuthor + '\'' +
                ", dataTimestamp=" + dataTimestamp +
                ", owner='" + owner + '\'' +
                ", marketDataType='" + marketDataType + '\'' +
                ", footerNote='" + footerNote + '\'' +
                ", articles=" + articles +
                ", articlesIds=" + articlesIds +
                ", dbsId=" + dbsId +
                '}';
    }
}
