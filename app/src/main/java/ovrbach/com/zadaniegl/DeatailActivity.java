package ovrbach.com.zadaniegl;
//:*

import android.content.Intent;
import android.content.res.Configuration;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;

import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.sql.SQLException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DeatailActivity extends AppCompatActivity {


    @BindView(R.id.deatail_strip)
    View deatailStrip;
    @BindView(R.id.nav_back)
    ImageView navBack;
    @BindView(R.id.nav_change_size)
    ImageView navChangeSize;
    @BindView(R.id.nav_translate)
    ImageView nav_invert;
    @BindView(R.id.nav_invert)
    ImageView navMore;
    @BindView(R.id.deatail_cat)
    TextView deatailCat;
    @BindView(R.id.deatail_love)
    TextView deatailLove;
    @BindView(R.id.deatail_zing)
    TextView deatailZing;
    @BindView(R.id.deatail_image)
    ImageView deatailImage;
    @BindView(R.id.navigation)
    BottomNavigationView navigation;
    @BindView(R.id.deatail_text)
    TextView deatailText;
    private FirebaseAnalytics mFirebaseAnalytics;
    private DataSource dataSource;
    private NightModeHelper mNmode;
    private Article a;

    //todo binding
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
mNmode=new NightModeHelper(this,R.style.AppTheme_NoActionBar);
        setContentView(R.layout.detail);




        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putLong("article_opened",a.getNid());
        mFirebaseAnalytics.logEvent("article", bundle);



        //navigation.set

        BottomNavigationViewHelper.disableShiftMode(navigation);
//todo odtranit shift
        navigation.setItemIconTintList(null);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_book:

                        Bundle params = new Bundle();
                        params.putLong("article_booked_id", a.getNid());
                        mFirebaseAnalytics.logEvent("article", params);

                        break;
                    case R.id.navigation_like:

                        Bundle params2 = new Bundle();
                        params2.putLong("article_liked", a.getNid());
                        mFirebaseAnalytics.logEvent("article", params2);

                        //if (item.getIcon().getState())
                        int[] state = new int[]{android.R.attr.state_checked};
                        item.getIcon().setState(state);
                        deatailLove.setText(Integer.valueOf(String.valueOf(deatailLove.getText()))+1+"");
                        break;
                    case R.id.navigation_share:
                        Bundle params3 = new Bundle();
                        params3.putLong("article_share", a.getNid());
                        mFirebaseAnalytics.logEvent("article", params3);

                        mFirebaseAnalytics.setUserProperty("shared_article",a.getNid()+"");

                        System.out.println(a);

                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, a.getLinkOneTitle());
                        i.putExtra(Intent.EXTRA_TEXT, a.getShareLink());
                        startActivity(Intent.createChooser(i, "Share URL"));

                        break;
                }
                return true;
            }
        });

//navigation.getMenu().getItem(1).



        deatailText.setText(Html.fromHtml(a.getBody()));
        deatailCat.setText(a.getHeadline());
        deatailLove.setText(40 + "");
        deatailZing.setText(a.getHeadline());

        Glide.with(this)
                .load(a.getLeaderImage())
                .into(deatailImage);


    }

    @OnClick({R.id.nav_back, R.id.nav_change_size, R.id.nav_translate, R.id.nav_invert})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.nav_back:
                onBackPressed();
                break;
            case R.id.nav_change_size:
                break;
            case R.id.nav_translate:

                //todo aj naspat, pozriet kniznice
                Locale locale = new Locale("zh");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config,
                        getBaseContext().getResources().getDisplayMetrics());

                this.setContentView(R.layout.detail);

                break;
            case R.id.nav_invert:
                mNmode.toggle();
                break;
        }
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
        dataSource = new DataSource(this);
        try {
            dataSource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Bundle b = getIntent().getExtras();
        Long l = b.getLong("NIB");
        a = dataSource.getArticlesById(l);

        Glide.with(this)
                .load(a.getLeaderImage())
                .into(deatailImage);
    }
}
